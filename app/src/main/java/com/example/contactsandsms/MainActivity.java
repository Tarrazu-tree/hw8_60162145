package com.example.contactsandsms;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private ImageView contactImageView, messageImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        contactImageView = findViewById(R.id.contactsImageView);
        messageImageView = findViewById(R.id.messageImageView);

        contactImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayContacts(v);
            }
        });

        messageImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplaySMS(v);
            }
        });
    }

    public void DisplayContacts(View view) {
        Intent intent = new Intent(this, DisplayContactActivity.class);
        startActivity(intent);
    }

    public void DisplaySMS(View view) {
        Intent intent = new Intent(this, DisplaySMSActivity.class);
        startActivity(intent);
    }
}
